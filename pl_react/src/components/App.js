import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Header from './Header';
import Menu from './Menu';
import Home from './pages/Home';
import Atoms from './pages/01_Atoms';
import Molecules from './pages/02_Molecules';
import Organisms from './pages/03_Organisms';
import Templates from './pages/04_Templates';


const App = () => {
    return (

      <div className="container__page">
      <Header />
        <BrowserRouter>
            <div className="content-flex">
                <div className="content-flex__sidebar">
                    <Menu />
                </div>
                <main className="content-flex__main-content ">
                        <Route path="/" exact component={Home} />
                        <Route path="/atoms" exact component={Atoms} />
                        <Route path="/molecules" exact component={Molecules} />
                        <Route path="/organisms" exact component={Organisms} />
                        <Route path="/template" exact component={Templates} />
                </main>
            </div>
        </BrowserRouter>
      </div>
    );
}


export default App;
