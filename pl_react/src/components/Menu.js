import React from 'react';
import { Link } from 'react-router-dom';

const Menu = () => {
    return (
        <nav className="container__side-bar">
            <ul>
                <li>
                <Link to="/" className="">Home</Link>
                </li>
                <li>
                <Link to="/atoms" className="">Atoms</Link>
                </li>
                <li>
                <Link to="/molecules" className="">Molecules</Link>
                </li>
                <li>
                <Link to="/organisms" className="">Organisms</Link>
                </li>
                <li>
                <Link to="/template" className="">Templates</Link>
                </li>
            </ul>
        </nav>
    );
};

export default Menu;
