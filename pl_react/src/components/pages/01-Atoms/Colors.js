import React from 'react';
import Color from './Color';

const Colors = () => {
    return (
        <section className="wrap">
            <h2 className="">Colors</h2>
              <div className="grid-4">
                  <div className="color__primary-l color-box">
                      color__primary-l
                  </div>
                  <div className="color__secondary-l color-box">
                      color__secondary-l
                  </div>
                  <div className="color__thrird-l color-box">
                      color__thrird-l
                  </div>
                  <div className="color__dark-l color-box">
                      color__dark-l
                  </div>
                  <div className="color__primary-d color-box">
                      color__primary-d
                  </div>
                  <div className="color__secondary-d color-box">
                      color__secondary-d
                  </div>
                  <div className="color__thrird-d color-box">
                      color__thrird-d
                  </div>
                  <div className="color__light-d color-box">
                      color__light-d
                  </div>
              </div>
        </section>
    );
};

export default Colors;
