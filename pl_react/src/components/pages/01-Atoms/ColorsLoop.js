import React from 'react';
import { connect } from 'react-redux'

// import Color from './Color';


class ColorsList extends React.Component {
    renderList() {
        return this.props.colors.map((color) => {
            return (
                <div className="item" key={color.name}>
                <div className="{color.name} color-box">
                    {color.name}
                </div>
                </div>
            )
        })
    }
    render() {
    console.log(this.props.colors);
        return (
            <section className="wrap">
                <h2 className="">Colors</h2>
                  <div className="grid-3">
                      {this.renderList()}
                  </div>
            </section>
        );
    };
};

const mapStateToPtops = state => {
    return { colors: state.colors };
}

export default connect(mapStateToPtops)(ColorsList);
