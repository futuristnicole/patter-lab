import React from 'react';
import ColorsList from './01-Atoms/Colors';

const Atoms = () => {
    return (
        <div>
        <h1>Atoms</h1>
            <ColorsList />
        </div>

    );
};

export default Atoms;
