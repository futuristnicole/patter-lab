import { combineReducers } from 'redux';

const colorsReducer = () => {
        return [
            { name: "color__primary-l" },
            { name: "color__secondary-l" },
            { name: "color__thrird-l" },
            { name: "color__dark-l" },
            { name: "color__primary-d" },
            { name: "color__secondary-d" },
            { name: "color__thrird-d" },
            { name: "color__light-d" },
        ]
};

export default combineReducers({
    colors: colorsReducer
});
